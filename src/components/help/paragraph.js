export const ParagraphText = ({ text }) => {
  return (
    <p className="mt-4 text-sm sm:text-base text-black-900 font-normal">
      {text}
    </p>
  );
};
