export const ParagraphTitleText = ({ text }) => {
  return <p className="mt-8 sm:mt-16 text-sm sm:text-base text-black-900 font-normal">{text}</p>;
};
