import React, { useState } from "react";
import "./sidebar.css";
import logo from "../assets/images/logo.png";
import orn1 from "../assets/ornament/card-top-left.svg";
import orn2 from "../assets/ornament/card-bot-right.svg";
import { Link } from "react-router-dom";
import utils from "../utility/utils";
import { XIcon } from "@heroicons/react/solid";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Start_TourSidebarAction } from "../redux/action/sidebar";
import Tour from "reactour";
import { SidebarStep } from "../tour-step/sidebar";

const Sidebar = ({ setOpenSidebar, sidebarOpen, verified }) => {
  const [path, setPath] = useState(window.location.pathname);
  const innerWidth = window.innerWidth;
  const dispatch = useDispatch();
  const req_counter_state = useSelector(
    (state) => state.RequestCounter.req_counter
  );
  const [tour, setTour] = useState(false);

  const handlePath = () => {
    let pathname = window.location.pathname;
    setPath(pathname);
    if (innerWidth < 768) setOpenSidebar(false);
  };

  const handleDone = () => {
    dispatch(Start_TourSidebarAction({ tour_sidebar: true }));
    setTour(false);
  };

  useEffect(() => {
    const local_tour_sidebar = JSON.parse(localStorage.getItem("tour_sidebar"));
    setTour(local_tour_sidebar === null && !local_tour_sidebar);
  }, []);

  return (
    <>
      <Tour
        isOpen={tour}
        rounded={8}
        showCloseButton={false}
        closeWithMask={false}
        steps={SidebarStep}
        disableDotsNavigation
        lastStepNextButton={
          <div
            className="btn__primary py-1 px-4 rounded-md"
            onClick={handleDone}
          >
            Selesai
          </div>
        }
      />
      <div className={sidebarOpen ? "loaded sidebar" : "sidebar"}>
        <div
          className="sm:hidden absolute right-3 rounded-md p-1 border border-blue-500 cursor-pointer"
          onClick={() => setOpenSidebar(false)}
        >
          <XIcon className="text-blue-500" width="20px" />
        </div>

        <div className="sidebar__logo sm:pt-0 pt-4">
          <img src={logo} alt="unknown" />
        </div>

        <div className="sidebar__menu">
          <div onClick={handlePath}>
            <Link to="/dashboard">
              <div className="sidebar__item">
                <button
                  className={utils.classNames(
                    path === "/dashboard" ? "active" : "",
                    "flex items-center"
                  )}
                >
                  <svg
                    width="24"
                    height="24"
                    className="mr-3"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M9.42105 17.9996H7C5.89543 17.9996 5 17.1042 5 15.9996V9.9195C5 9.33565 5.25513 8.78095 5.69842 8.40099L10.6984 4.11528C11.4474 3.47329 12.5526 3.47329 13.3016 4.11528L18.3016 8.40099C18.7449 8.78095 19 9.33565 19 9.9195V15.9996C19 17.1042 18.1046 17.9996 17 17.9996H14.5789M9.42105 17.9996V12.4996C9.42105 11.6712 10.0926 10.9996 10.9211 10.9996H13.0789C13.9074 10.9996 14.5789 11.6712 14.5789 12.4996V17.9996M9.42105 17.9996H14.5789"
                      stroke="#347AE2"
                      strokeWidth="1.3"
                    />
                  </svg>
                  Dashboard
                </button>
              </div>
            </Link>
          </div>

          <div onClick={handlePath}>
            <Link to="/dashboard/signature">
              <div className="sidebar__item" data-tour="signature">
                <button
                  className={utils.classNames(
                    path === "/dashboard/signature" ? "active" : "",
                    "flex items-center"
                  )}
                >
                  <svg
                    width="24"
                    height="24"
                    className="mr-3"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13.782 17.4272C13.7554 17.4272 13.7287 17.4218 13.7018 17.4109C13.6325 17.3837 13.5736 17.3294 13.5522 17.2534C13.4989 17.0743 13.0234 15.4786 13.2852 14.7241L16.7096 4.96985C16.9714 4.23174 17.7194 3.83008 18.3818 4.0689C19.0443 4.30772 19.3756 5.10567 19.1138 5.84377L15.6893 15.598C15.4221 16.3524 14.0598 17.2806 13.9102 17.3837C13.8728 17.4163 13.8247 17.4272 13.782 17.4272ZM17.9866 4.48683C17.6393 4.48683 17.3027 4.74192 17.1638 5.13269L13.7392 14.8869C13.5951 15.2995 13.7766 16.2167 13.9209 16.7812C14.3857 16.4392 15.0962 15.8422 15.2405 15.4296L18.665 5.67555C18.836 5.19239 18.6383 4.67684 18.2217 4.52475C18.1469 4.5031 18.0666 4.48683 17.9866 4.48683Z"
                      fill="#347AE2"
                      stroke="#347AE2"
                      strokeWidth="0.6"
                    />
                    <path
                      d="M13.6011 17.9426C13.5744 17.9426 13.5477 17.9371 13.521 17.9263C13.3981 17.8829 13.3286 17.7417 13.3767 17.6114L13.5584 17.0958C13.6011 16.9709 13.74 16.9003 13.8683 16.9492C13.991 16.9927 14.0605 17.1337 14.0124 17.264L13.8309 17.7797C13.7935 17.8829 13.6973 17.9426 13.6011 17.9426Z"
                      fill="#347AE2"
                      stroke="#347AE2"
                      strokeWidth="0.6"
                    />
                    <path
                      d="M17.0192 11.3421C16.9925 11.3421 16.9658 11.3367 16.9391 11.3258L14.9838 10.6147C14.8609 10.5713 14.7915 10.4301 14.8396 10.2999L15.08 9.61049C15.1014 9.55079 15.1441 9.50198 15.2029 9.46944C15.2615 9.43676 15.3258 9.43676 15.3844 9.45854L17.3398 10.1696C17.3986 10.1913 17.4466 10.2347 17.4787 10.2944C17.5054 10.3541 17.5108 10.4193 17.4894 10.479L17.249 11.1683C17.2116 11.2824 17.1154 11.3421 17.0192 11.3421ZM15.3684 10.2402L16.8696 10.7829L16.9498 10.555L15.4485 10.0122L15.3684 10.2402Z"
                      fill="#347AE2"
                      stroke="#347AE2"
                      strokeWidth="0.6"
                    />
                    <path
                      d="M19.242 7.57541C19.2153 7.57541 19.1887 7.57003 19.162 7.55914L18.3659 7.27151C18.3071 7.24972 18.2591 7.20629 18.227 7.14658C18.2003 7.08688 18.1949 7.0218 18.2163 6.96209L18.601 5.86562C18.6224 5.80592 18.6651 5.75711 18.7239 5.72442C18.7827 5.69726 18.8468 5.69188 18.9055 5.71367L19.7015 6.0013C19.8244 6.04474 19.8939 6.18579 19.8458 6.31609L19.4611 7.41257C19.4397 7.47227 19.397 7.52108 19.3382 7.55376C19.3115 7.57003 19.2794 7.57541 19.242 7.57541ZM18.7506 6.89687L19.0979 7.0218L19.3223 6.38669L18.975 6.26191L18.7506 6.89687Z"
                      fill="#347AE2"
                      stroke="#347AE2"
                      strokeWidth="0.6"
                    />
                    <path
                      d="M18.0874 10.8594C18.0607 10.8594 18.034 10.854 18.0072 10.8431C17.8844 10.7997 17.8149 10.6586 17.863 10.5283L19.5298 5.78422C19.5726 5.65943 19.7115 5.58883 19.8397 5.63764C19.9626 5.68108 20.0321 5.82228 19.984 5.95258L18.3171 10.6966C18.2797 10.7997 18.1889 10.8594 18.0874 10.8594Z"
                      fill="#347AE2"
                      stroke="#347AE2"
                      strokeWidth="0.6"
                    />
                    <path
                      d="M11.9664 18.8659C11.913 18.8659 11.8649 18.855 11.833 18.8387C11.6673 18.7681 11.6032 18.589 11.5391 18.4208C11.4429 18.1711 11.3895 18.0788 11.2826 18.0788C11.0636 18.0788 10.7964 18.2579 10.5348 18.4317C10.2835 18.5999 10.0272 18.7736 9.74399 18.8387C9.5944 18.8768 9.44481 18.8387 9.32733 18.7409C9.10295 18.5511 9.01745 18.1276 8.98533 17.7694C8.77707 17.9322 8.49916 18.1276 8.16258 18.3067C7.17968 18.8387 6.18065 18.9799 5.2724 18.7139C4.55643 18.5021 4.14505 17.791 4.10765 16.7055C4.08096 15.8424 4.30006 14.746 4.69003 13.7798C4.88244 13.3022 5.41129 12.1569 6.10043 11.9018C6.74161 11.6683 7.37723 11.88 7.94362 12.5151C8.34429 12.9602 8.56339 13.465 8.57396 13.4867C8.62748 13.6116 8.57396 13.7526 8.4512 13.807C8.3283 13.8613 8.18942 13.807 8.13589 13.6822C8.12519 13.655 7.36125 11.9615 6.26601 12.3576C5.91887 12.4826 5.47538 13.1122 5.13352 13.9589C4.76482 14.8654 4.56185 15.8859 4.58854 16.6838C4.60453 17.2862 4.76482 18.0463 5.40601 18.2361C7.30786 18.8006 9.02273 17.0855 9.03886 17.0692C9.10823 16.9986 9.21514 16.977 9.30592 17.0203C9.39671 17.0583 9.45551 17.1506 9.45023 17.2537C9.42882 17.791 9.53031 18.2905 9.62651 18.3611C9.62651 18.3611 9.62651 18.3611 9.6318 18.3611C9.82421 18.3176 10.0379 18.1711 10.2623 18.0245C10.5774 17.8128 10.9086 17.5903 11.272 17.5903C11.7313 17.5903 11.8809 17.9973 11.9771 18.247C11.9878 18.2796 12.0038 18.3176 12.02 18.3502C12.1801 18.3067 12.3458 18.3067 12.4954 18.3123C12.5969 18.3123 12.7785 18.3176 12.8159 18.2851C12.9121 18.1873 13.0617 18.1873 13.1579 18.2851C13.2539 18.3827 13.2539 18.5348 13.1579 18.6324C12.9816 18.8115 12.7198 18.8062 12.4954 18.8062C12.3564 18.8062 12.2122 18.8006 12.116 18.8441C12.0626 18.855 12.0093 18.8659 11.9664 18.8659Z"
                      fill="#347AE2"
                      stroke="#347AE2"
                      strokeWidth="0.6"
                    />
                    <path
                      d="M16.7631 20.0002H4.24037C4.10677 20.0002 4 19.8917 4 19.7561C4 19.6204 4.10677 19.5117 4.24037 19.5117H16.7631C16.8966 19.5117 17.0035 19.6204 17.0035 19.7561C17.0035 19.8917 16.8966 20.0002 16.7631 20.0002Z"
                      fill="#347AE2"
                    />
                  </svg>
                  My Signature
                </button>
              </div>
            </Link>
          </div>

          <div onClick={handlePath}>
            <Link to="/dashboard/request">
              <div className="sidebar__item">
                <button
                  className={utils.classNames(
                    path === "/dashboard/request" ? "active" : "",
                    "flex items-center justify-between"
                  )}
                >
                  <div className="flex">
                    <svg
                      width="24"
                      height="24"
                      className="mr-3"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g clipPath="url(#clip0)">
                        <path
                          d="M4.00313 19.6716C4.00313 14.4957 4.00313 9.31978 4 4.14386C4 4.02502 4.02501 4 4.14381 4C7.95467 4.00313 11.7687 4.00313 15.5795 4.00313C15.8202 4.11572 15.9078 4.30336 15.9078 4.56919C15.9046 9.51994 15.9046 14.4676 15.9046 19.4183C15.9046 19.8249 15.7358 19.9969 15.3294 19.9969C11.7436 19.9969 8.15787 19.9969 4.5721 20C4.30324 20 4.11567 19.9124 4.00313 19.6716ZM14.9418 12.0031C14.9418 9.72635 14.9418 7.44644 14.9418 5.16966C14.9418 4.95074 14.9418 4.95074 14.7261 4.95074C11.5404 4.95074 8.3517 4.95074 5.16608 4.95074C4.9535 4.95074 4.9535 4.95074 4.9535 5.17279C4.9535 9.71697 4.9535 14.2643 4.9535 18.8084C4.9535 19.0336 4.9535 19.0336 5.17859 19.0336C8.36108 19.0336 11.5404 19.0336 14.7229 19.0336C14.9449 19.0336 14.9449 19.0336 14.9449 18.8053C14.9418 16.5379 14.9418 14.2705 14.9418 12.0031Z"
                          fill="#347AE2"
                        />
                        <path
                          d="M18.1056 4.00293C18.2932 4.00293 18.4807 4.00293 18.6683 4.00293C19.4718 4.16556 20.0095 4.85672 20.0001 5.73553C19.9751 7.84343 19.9907 9.9482 19.9907 12.0561C19.9907 13.1382 19.9876 14.2203 19.9907 15.3024C19.9907 15.5369 19.9032 15.7089 19.6875 15.7965C19.5874 15.8372 19.5999 15.8935 19.6281 15.9685C19.7344 16.2625 19.8406 16.5565 19.9469 16.8473C19.997 16.9818 20.0188 17.1226 19.9563 17.257C19.5812 18.0796 19.206 18.9052 18.8246 19.7246C18.7246 19.9373 18.512 20.0311 18.2932 19.9873C18.1025 19.9498 17.9931 19.8153 17.9149 19.6464C17.5648 18.8739 17.2084 18.1046 16.8551 17.3352C16.7801 17.1695 16.7707 17.01 16.8363 16.838C16.9458 16.5502 17.0458 16.2625 17.1521 15.9748C17.1834 15.8903 17.1834 15.834 17.0833 15.7903C16.8676 15.6964 16.7832 15.5213 16.7832 15.2899C16.7832 12.053 16.7832 8.81919 16.7832 5.58229C16.7832 5.32897 16.8426 5.08815 16.9583 4.86298C17.2021 4.39073 17.5991 4.12177 18.1056 4.00293ZM19.031 11.3837C19.031 10.3016 19.031 9.2195 19.031 8.13741C19.031 7.91536 19.031 7.91536 18.8184 7.91536C18.5433 7.91536 18.2682 7.91536 17.9931 7.91536C17.7742 7.91536 17.7648 7.92474 17.7648 8.14366C17.7648 9.09128 17.7648 10.0358 17.7648 10.9834C17.7648 12.2 17.7648 13.4165 17.7648 14.6331C17.7648 14.8458 17.7773 14.8583 17.9837 14.8583C18.265 14.8583 18.5464 14.8583 18.8246 14.8583C19.0278 14.8583 19.031 14.852 19.0341 14.6456C19.031 13.5573 19.031 12.472 19.031 11.3837ZM17.7617 6.16087C17.7617 6.37353 17.7617 6.5862 17.7617 6.80199C17.7617 6.89269 17.7867 6.95524 17.893 6.95524C18.2306 6.95524 18.5683 6.95524 18.9059 6.95524C18.9903 6.95524 19.031 6.91458 19.031 6.83014C19.031 6.41419 19.0435 5.99824 19.0247 5.58229C19.0091 5.21325 18.7121 4.94116 18.3744 4.9568C18.0212 4.97244 17.7711 5.24452 17.7648 5.63233C17.7586 5.80434 17.7617 5.9826 17.7617 6.16087ZM18.387 15.8434C18.2744 15.8247 18.215 15.8747 18.1806 15.981C18.0681 16.3 17.9524 16.6159 17.8367 16.9349C17.8055 17.0193 17.8117 17.0944 17.8492 17.1726C18.0087 17.5104 18.1619 17.8512 18.3182 18.1921C18.3338 18.2265 18.3401 18.2828 18.3901 18.2828C18.4401 18.2828 18.4464 18.2265 18.462 18.1921C18.6183 17.8512 18.7715 17.5104 18.9309 17.1726C18.9684 17.0913 18.9747 17.0162 18.9434 16.9318C18.8278 16.6159 18.709 16.2969 18.5995 15.9779C18.562 15.8716 18.4995 15.8278 18.387 15.8434Z"
                          fill="#347AE2"
                        />
                      </g>
                      <defs>
                        <clipPath id="clip0">
                          <rect
                            width="16"
                            height="16"
                            fill="white"
                            transform="translate(4 4)"
                          />
                        </clipPath>
                      </defs>
                    </svg>
                    My Request
                  </div>
                  {req_counter_state > 0 && (
                    <span
                      className={utils.classNames(
                        path === "/dashboard/request"
                          ? "bg-white text-blue-500"
                          : "bg-blue-500 text-white",
                        "flex items-center justify-center font-bold rounded-sm h-6 w-6"
                      )}
                    >
                      {req_counter_state}
                    </span>
                  )}
                </button>
              </div>
            </Link>
          </div>

          <div onClick={handlePath}>
            <Link to="/dashboard/collaboration">
              <div className="sidebar__item" data-tour="collaboration">
                <button
                  className={utils.classNames(
                    path === "/dashboard/collaboration" ? "active" : "",
                    "flex items-center"
                  )}
                >
                  <svg
                    width="24"
                    height="24"
                    className="mr-3"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M9.71289 18.2542C9.91929 16.6288 11.3228 13.3779 15.2858 13.3779C19.2487 13.3779 20.6522 16.6288 20.8586 18.2542"
                      stroke="#347AE2"
                      strokeWidth="1.3"
                      strokeLinecap="round"
                    />
                    <circle
                      cx="15.2858"
                      cy="8.64126"
                      r="2.22915"
                      stroke="#347AE2"
                      strokeWidth="1.3"
                    />
                    <path
                      d="M4.14062 14.3103C4.27661 13.2394 5.20129 11.0977 7.81217 11.0977C9.7135 11.0977 10.8281 12.6384 10.8281 12.6384"
                      stroke="#347AE2"
                      strokeWidth="1.2"
                      strokeLinecap="round"
                    />
                    <circle
                      cx="7.87874"
                      cy="7.83577"
                      r="1.33577"
                      stroke="#347AE2"
                    />
                  </svg>
                  Collaboration
                </button>
              </div>
            </Link>
          </div>

          <div onClick={handlePath}>
            <Link to="/dashboard/contact">
              <div className="sidebar__item">
                <button
                  className={utils.classNames(
                    path === "/dashboard/contact" ? "active" : "",
                    "flex items-center"
                  )}
                >
                  <svg
                    width="24"
                    height="24"
                    className="mr-3"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M17.3333 20.666H5.66667C4.75 20.666 4 19.916 4 18.9993V7.33268C4 6.41602 4.75 5.66602 5.66667 5.66602H17.3333C18.25 5.66602 19 6.41602 19 7.33268V18.9993C19 19.916 18.25 20.666 17.3333 20.666Z"
                      stroke="#347AE2"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M14.834 4V5.66667"
                      stroke="#347AE2"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M8.16602 4V5.66667"
                      stroke="#347AE2"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M11.5 14C12.8807 14 14 12.8807 14 11.5C14 10.1193 12.8807 9 11.5 9C10.1193 9 9 10.1193 9 11.5C9 12.8807 10.1193 14 11.5 14Z"
                      stroke="#347AE2"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M15.6673 17.75C14.5007 16.9167 13.084 16.5 11.5007 16.5C9.91732 16.5 8.50065 17 7.33398 17.75"
                      stroke="#347AE2"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                  My Contact
                </button>
              </div>
            </Link>
          </div>

          <div onClick={handlePath}>
            <Link to="/dashboard/profile">
              <div className="sidebar__item relative">
                {!verified && (
                  <div className="p-1 bg-yellow-400 absolute right-2 top-2 rounded-lg"></div>
                )}
                <button
                  className={utils.classNames(
                    path === "/dashboard/profile" ? "active" : "",
                    "flex items-center"
                  )}
                >
                  <svg
                    width="24"
                    height="24"
                    className="mr-3"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M12.0002 14.182C13.2052 14.182 14.182 13.2052 14.182 12.0002C14.182 10.7952 13.2052 9.81836 12.0002 9.81836C10.7952 9.81836 9.81836 10.7952 9.81836 12.0002C9.81836 13.2052 10.7952 14.182 12.0002 14.182Z"
                      stroke="#347AE2"
                      strokeWidth="1.3"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M17.3818 14.1818C17.285 14.4012 17.2561 14.6445 17.2989 14.8804C17.3417 15.1164 17.4542 15.3341 17.6218 15.5055L17.6655 15.5491C17.8007 15.6842 17.908 15.8446 17.9812 16.0212C18.0544 16.1978 18.0921 16.387 18.0921 16.5782C18.0921 16.7693 18.0544 16.9586 17.9812 17.1352C17.908 17.3118 17.8007 17.4722 17.6655 17.6073C17.5304 17.7425 17.3699 17.8498 17.1934 17.923C17.0168 17.9962 16.8275 18.0339 16.6364 18.0339C16.4452 18.0339 16.2559 17.9962 16.0794 17.923C15.9028 17.8498 15.7424 17.7425 15.6073 17.6073L15.5636 17.5636C15.3922 17.396 15.1745 17.2835 14.9386 17.2407C14.7027 17.1979 14.4594 17.2268 14.24 17.3236C14.0249 17.4158 13.8414 17.5689 13.7122 17.764C13.583 17.9591 13.5137 18.1878 13.5127 18.4218V18.5455C13.5127 18.9312 13.3595 19.3012 13.0867 19.574C12.8139 19.8468 12.444 20 12.0582 20C11.6724 20 11.3024 19.8468 11.0297 19.574C10.7569 19.3012 10.6036 18.9312 10.6036 18.5455V18.48C10.598 18.2393 10.5201 18.0058 10.38 17.81C10.2399 17.6141 10.0442 17.4649 9.81818 17.3818C9.59882 17.285 9.3555 17.2561 9.11957 17.2989C8.88365 17.3417 8.66595 17.4542 8.49455 17.6218L8.45091 17.6655C8.31582 17.8007 8.1554 17.908 7.97882 17.9812C7.80224 18.0544 7.61297 18.0921 7.42182 18.0921C7.23067 18.0921 7.04139 18.0544 6.86481 17.9812C6.68824 17.908 6.52782 17.8007 6.39273 17.6655C6.25749 17.5304 6.1502 17.3699 6.077 17.1934C6.00381 17.0168 5.96613 16.8275 5.96613 16.6364C5.96613 16.4452 6.00381 16.2559 6.077 16.0794C6.1502 15.9028 6.25749 15.7424 6.39273 15.6073L6.43636 15.5636C6.60403 15.3922 6.7165 15.1745 6.75928 14.9386C6.80205 14.7027 6.77317 14.4594 6.67636 14.24C6.58417 14.0249 6.43109 13.8414 6.23597 13.7122C6.04085 13.583 5.81221 13.5137 5.57818 13.5127H5.45455C5.06878 13.5127 4.69881 13.3595 4.42603 13.0867C4.15325 12.8139 4 12.444 4 12.0582C4 11.6724 4.15325 11.3024 4.42603 11.0297C4.69881 10.7569 5.06878 10.6036 5.45455 10.6036H5.52C5.76072 10.598 5.99419 10.5201 6.19004 10.38C6.38589 10.2399 6.53507 10.0442 6.61818 9.81818C6.71499 9.59882 6.74387 9.3555 6.70109 9.11957C6.65832 8.88365 6.54585 8.66595 6.37818 8.49455L6.33455 8.45091C6.19931 8.31582 6.09202 8.1554 6.01882 7.97882C5.94562 7.80224 5.90795 7.61297 5.90795 7.42182C5.90795 7.23067 5.94562 7.04139 6.01882 6.86481C6.09202 6.68824 6.19931 6.52782 6.33455 6.39273C6.46963 6.25749 6.63005 6.1502 6.80663 6.077C6.98321 6.00381 7.17249 5.96613 7.36364 5.96613C7.55479 5.96613 7.74406 6.00381 7.92064 6.077C8.09722 6.1502 8.25764 6.25749 8.39273 6.39273L8.43636 6.43636C8.60777 6.60403 8.82547 6.7165 9.06139 6.75928C9.29731 6.80205 9.54064 6.77317 9.76 6.67636H9.81818C10.0333 6.58417 10.2167 6.43109 10.346 6.23597C10.4752 6.04085 10.5445 5.81221 10.5455 5.57818V5.45455C10.5455 5.06878 10.6987 4.69881 10.9715 4.42603C11.2443 4.15325 11.6142 4 12 4C12.3858 4 12.7557 4.15325 13.0285 4.42603C13.3013 4.69881 13.4545 5.06878 13.4545 5.45455V5.52C13.4555 5.75403 13.5248 5.98267 13.654 6.17779C13.7833 6.37291 13.9667 6.52599 14.1818 6.61818C14.4012 6.71499 14.6445 6.74387 14.8804 6.70109C15.1164 6.65832 15.3341 6.54585 15.5055 6.37818L15.5491 6.33455C15.6842 6.19931 15.8446 6.09202 16.0212 6.01882C16.1978 5.94562 16.387 5.90795 16.5782 5.90795C16.7693 5.90795 16.9586 5.94562 17.1352 6.01882C17.3118 6.09202 17.4722 6.19931 17.6073 6.33455C17.7425 6.46963 17.8498 6.63005 17.923 6.80663C17.9962 6.98321 18.0339 7.17249 18.0339 7.36364C18.0339 7.55479 17.9962 7.74406 17.923 7.92064C17.8498 8.09722 17.7425 8.25764 17.6073 8.39273L17.5636 8.43636C17.396 8.60777 17.2835 8.82547 17.2407 9.06139C17.1979 9.29731 17.2268 9.54064 17.3236 9.76V9.81818C17.4158 10.0333 17.5689 10.2167 17.764 10.346C17.9591 10.4752 18.1878 10.5445 18.4218 10.5455H18.5455C18.9312 10.5455 19.3012 10.6987 19.574 10.9715C19.8468 11.2443 20 11.6142 20 12C20 12.3858 19.8468 12.7557 19.574 13.0285C19.3012 13.3013 18.9312 13.4545 18.5455 13.4545H18.48C18.246 13.4555 18.0173 13.5248 17.8222 13.654C17.6271 13.7833 17.474 13.9667 17.3818 14.1818V14.1818Z"
                      stroke="#347AE2"
                      strokeWidth="1.3"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                  Settings
                </button>
              </div>
            </Link>
          </div>
        </div>

        <div className="sidebar__footer_wrapper">
          <div className="sidebar__footer">
            <h5 className="mb-3">Try Mobile App Version</h5>
            <button>Download</button>

            <img src={orn1} alt="unknown" className="sidebar__footer_orn1" />
            <img src={orn2} alt="unknown" className="sidebar__footer_orn2" />
          </div>
        </div>
      </div>

      {innerWidth < 768 && sidebarOpen && (
        <div
          className="outside__layer bg-black opacity-20"
          onClick={() => setOpenSidebar(false)}
        />
      )}
    </>
  );
};

export default Sidebar;
